<?php
/**
 * Created by PhpStorm.
 * User: shabbir
 * Date: 3/25/17
 * Time: 5:53 PM
 */

namespace App\Events;

use App\Order;
use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class OrderConsolidated implements ShouldBroadcast
{
    use SerializesModels;

    public $order;

    /**
     * OrderConsolidated constructor.
     * @param Order $order
     */
    public function __construct(Order $order)
    {
        $this->order = $order;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return Channel|array
     */
    public function broadcastOn()
    {
        return new Channel('orders');
    }

    public function broadcastWith()
    {
        return ['rack' => $this->order->rack, 'order'=> $this->order, 'items_count'=> $this->order->itemsCount];
    }
}