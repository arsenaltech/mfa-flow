<?php
/**
 * Created by PhpStorm.
 * User: shabbir
 * Date: 3/25/17
 * Time: 5:53 PM
 */

namespace App\Events;

use App\Order;
use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class OrderReleased implements ShouldBroadcast
{
    use SerializesModels;

    public $order;

    /**
     * OrderConsolidated constructor.
     * @param Order $order
     */
    public function __construct($order_id)
    {
        $this->order = Order::find($order_id);
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return Channel|array
     */
    public function broadcastOn()
    {
        return new Channel('orders');
    }

    public function broadcastWith()
    {
        return ['order'=> $this->order];
    }
}