<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SkuMapping extends Model
{
    protected $table = 'sku_mapping';
}
