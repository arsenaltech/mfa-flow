<?php
/**
 * Created by PhpStorm.
 * User: karan.darji
 * Date: 11/6/2015
 * Time: 9:38 AM
 */

namespace App;

class OrderStatusManagement
{

    public function getCalculatedOrderStatusFromItems($order_id)
    {
        $items = OrderItem::with('status')
        ->whereOrderId($order_id)
        ->get();
        $min_item = $items->sort(function ($a, $b) {
            $fa = $a->status->order_status_id;
            $fb = $b->status->order_status_id;
            if ($fa === $fb) {
                return 0;
            }
            return ($fa > $fb) ? 1 : -1;
        })
        ->first();
        return $min_item->status;
    }
}
