<?php

namespace App;


use Carbon\Carbon;

class OrderStatusHistoryRepository
{
    protected $order_status_management;

    public function __construct(
        OrderStatusManagement $order_status_management
    ) {
    
        $this->order_status_management = $order_status_management;
    }


    public function storeStatusHistoryByItem(OrderItem $order_item, $other_info = [])
    {
        $user = \Auth::user();
        /*insert into order status history*/
        $order_status_history = new OrderStatusHistory();
        $order_status_history->order_id = $order_item->order_id;
        $order_status_history->order_status_id = $order_item->status_id;
        $order_status_history->date_added = Carbon::now('America/Los_Angeles');
        $order_status_history->comment = isset($other_info['comment']) ? $other_info['comment'] : null;
        $order_status_history->user_admin_id = $user->admin_id;
        $order_status_history->updated_by = $user->name;
        $order_status_history->customer_notified = 0;
        $order_status_history->item_id = $order_item->order_info_id;
        $order_item->statusHistory()->save($order_status_history);
        return $order_item;
    }


    public function updateOrderItemStatus($order_item_id, $status_id, $other_info)
    {
        /*update item status*/
        $order_item = OrderItem::find($order_item_id);
        $order_item->status_id = $status_id;
        $order_item->save();
        $this->storeStatusHistoryByItem($order_item, $other_info); /*insert into ordre */
    }

    public function saveStatusHistoryByOrder(Order $order, OrderStatus $order_status, $additional_parameter = [])
    {
        $user = \Auth::user();
        $order_status_history = new OrderStatusHistory();
        $order_status_history->order_id = $order->order_id;
        $order_status_history->order_status_id = $order_status->order_status_id;
        $order_status_history->date_added = Carbon::now('America/Los_Angeles');
        $order_status_history->comment = isset($additional_parameter['comment']) ? $additional_parameter['comment'] : null;
        $order_status_history->user_admin_id = $user->admin_id;
        $order_status_history->customer_notified = 0;
        $order_status_history->item_id = 0;
        $order->statusHistory()->save($order_status_history);
        return $order;
    }
}
