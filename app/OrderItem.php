<?php
/**
 * Created by PhpStorm.
 * User: shabbir
 * Date: 3/17/17
 * Time: 7:50 PM
 */

namespace App;


use Illuminate\Database\Eloquent\Model;

class OrderItem  extends Model
{
    protected $table = 'order_info';
    protected $primaryKey = 'order_info_id';
    public $timestamps = false;
    protected $appends=['quantity_to_consolidate'];


    public function order()
    {
        return $this->belongsTo(Order::class, 'order_id', 'order_id');
    }

    public function status()
    {
        return $this->hasOne(OrderStatus::class, 'order_status_id', 'status_id');
    }

    public function statusHistory()
    {
        return $this
            ->hasOne(OrderStatusHistory::class,  'item_id', 'order_info_id' )
            ->orderBy('order_status_history_id', 'DESC');
    }

    public function getQuantityToConsolidateAttribute() {
        return ($this->quantity > $this->quantity_consolidated) ? 1 : 0;
    }

}
