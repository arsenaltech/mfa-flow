<?php

namespace App;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\OrderStatusHistory
 *
 * @property integer $order_status_history_id
 * @property integer $order_id
 * @property integer $order_status_id
 * @property string $status_name
 * @property string $comment_date
 * @property string $comments
 * @property integer $user_id customer_id or admin_id
 * @property string $user_name
 * @property mixed $notify_customer
 * @property mixed $visibile_on_frontend
 * @property mixed $added_by_customer
 * @property string $additional_parameters uploaded files, etc
 * @property integer $order_item_id
 * @property integer $product_group_id
 * @property mixed $internal_note Field to check if the comment was marked as internal-note
 * @property string $comment_type 0-manually-added, 1-system-generated
 * @property-read \App\Models\Order $order
 * @method static \Illuminate\Database\Query\Builder|\App\Models\OrderStatusHistory whereOrderStatusHistoryId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\OrderStatusHistory whereOrderId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\OrderStatusHistory whereOrderStatusId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\OrderStatusHistory whereStatusName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\OrderStatusHistory whereCommentDate($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\OrderStatusHistory whereComments($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\OrderStatusHistory whereUserId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\OrderStatusHistory whereUserName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\OrderStatusHistory whereNotifyCustomer($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\OrderStatusHistory whereVisibileOnFrontend($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\OrderStatusHistory whereAddedByCustomer($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\OrderStatusHistory whereAdditionalParameters($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\OrderStatusHistory whereOrderItemId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\OrderStatusHistory whereProductGroupId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\OrderStatusHistory whereInternalNote($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\OrderStatusHistory whereCommentType($value)
 */
class OrderStatusHistory extends Model
{
    protected $table = 'order_status_history';
    protected $primaryKey = 'order_status_history_id';
    public $timestamps = false;

    public function order()
    {
        return $this->belongsTo('App\Models\Order', 'order_id', 'order_id');
    }
}
