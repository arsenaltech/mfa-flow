<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Messages\SlackMessage;
use Illuminate\Notifications\Notification;
use Illuminate\Support\Str;

class ShopifyOrder extends Notification
{
    use Queueable;

    private $missing_item;
    private $missing_order;
    private $store;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($missing_order, $missing_item, $store)
    {
        $this->missing_order = $missing_order;
        $this->missing_item = $missing_item;
        $this->store = $store;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param mixed $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail', 'slack'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param mixed $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $missing_order_count = count($this->missing_order);
        $missing_item_count = count($this->missing_item);

        return (new MailMessage)
            ->subject("Order import issues on ". $this->store[2])
            ->line('We have  ' . $missing_order_count . ' ' . Str::plural('order', $missing_order_count) . ' didnt push to MFA and ' . $missing_item_count . ' ' . Str::plural('item', $missing_order_count) . ' with count miss match on store ' . $this->store[2])
            ->attachData($this->array_to_csv($this->missing_order), "missing-order.csv")
            ->attachData($this->array_to_csv($this->missing_item), "missing-item.csv")
            ->line('Thank you for using our application!');
    }


    public function toSlack($notifiable)
    {
        $missing_order_count = count($this->missing_order);
        $missing_item_count = count($this->missing_item);

        return (new SlackMessage)
            ->content('We have  ' . $missing_order_count . ' ' . Str::plural('order', $missing_order_count) . ' didnt push to MFA and ' . $missing_item_count . ' ' . Str::plural('item', $missing_order_count) . ' with count miss match on store ')
            ->attachment(function ($attachment) use ($missing_order_count) {
                $attachment->title($this->store[2])
                    ->fields(
                            array_merge($this->array_to_slack($this->missing_order),$this->array_to_slack($this->missing_item, 'Missing Items:'))
                    );

            });;
    }

    /**
     * Get the array representation of the notification.
     *
     * @param mixed $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }


    private function array_to_csv($array, $header = 'Order id')
    {
        $csv = "$header\n";

        foreach ($array as $value) {
            $csv .= $value . "\n";
        }

        return $csv;
    }

    private function array_to_slack($array, $header = "Missing Order:")
    {
        $orders = [];

        foreach ($array as $value) {
            $orders["$header ".$value] = $value;
        }

        return $orders;
    }
}
