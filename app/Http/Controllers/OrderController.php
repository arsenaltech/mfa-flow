<?php

namespace App\Http\Controllers;

use App\Events\OrderConsolidated;
use App\Order;
use App\OrderItem;
use App\OrderStatus;
use App\OrderStatusHistoryRepository;
use App\OrderStatusManagement;
use App\Rack;
use Illuminate\Http\Request;

class OrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $order = Order::find($id);

        if($order == null) {
            $order = OrderItem::find($id)->order;
        }
        $order = $order->load([
            'rack',
            'status',
            'orderItems',
            'orderItems.status',

        ]);
        return $order;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function readyToShip(Request $request) {
        return Order::with('rack', 'itemsCount')
            ->whereRaw("order_id in (SELECT order_id from rack_numbers)")
            ->orderBy('ship_deadline')
            ->get();
    }

    public function addToRack(Request $request,
                              OrderStatusHistoryRepository $repo,
                              OrderStatusManagement $order_status_management) {
        $items = $request->get('items', []);
        $order_id = $request->get('order_id');
        /** @var Order $order */
        $order = Order::with('rack')->find($order_id);
        if($order->rack == null) {
            $order->assignRack();
        }
        foreach($items as $item_info) {
            $item_id = $item_info['item_id'];
            $quantity_to_consolidate = $item_info['quantity_to_consolidate'];
            $order_item = OrderItem::find($item_id);
            $order_item->quantity_consolidated = $order_item->quantity_consolidated + $quantity_to_consolidate;
            $order_item->save();
            if($order_item->quantity_consolidated < $order_item->quantity) {
                $status = config('mfa.PARTIALLY_IN_BIN');
            }
            else {
                $status = config('mfa.WAITING_IN_BIN');
            }
            $repo->updateOrderItemStatus(
                $item_id,
                $status,
                ['comment'=>'Added '.$quantity_to_consolidate.' to bin: '.$order->rack->rack_id]
            );

        }
        $minimum_status = $order_status_management->getCalculatedOrderStatusFromItems($order_id);
        Order::whereOrderId($order_id)->update(['order_status_id' => $minimum_status->order_status_id]);
        $order->load('itemsCount');
        event(new OrderConsolidated($order));
//        if($minimum_status->order_status_id == config('mfa.WAITING_IN_BIN')) {
//            event(new OrderConsolidated($order));
//        }
        return ['success'=> true, 'rack_id' => $order->rack->rack_id];
    }

    public function releaseRack(Request $request) {
        $order_id = $request->get('order_id');
        Rack::release($order_id);
    }

    public function updateStatus(OrderStatusHistoryRepository $repo, Request $request) {
        $order_id = $request->get('order_id');
        $status_id = $request->get('status_id');
        $order_status = OrderStatus::find($status_id);
        $order = Order::find($order_id);
        $order->order_status_id = $status_id;
        $order->save();
        $order->orderItems()->update(['status_id'=>$status_id]);
        $repo->saveStatusHistoryByOrder($order, $order_status, ['comment'=> 'Consolidated']);
        if($order->order_status_id == config('mfa.CONSOLIDATED')) {
            Rack::release($order_id);
        }

    }
}
