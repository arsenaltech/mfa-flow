<?php
/**
 * Created by PhpStorm.
 * User: shabbir
 * Date: 3/17/17
 * Time: 8:02 PM
 */

namespace App;


use Illuminate\Database\Eloquent\Model;

class OrderStatus extends Model
{
    protected $table = 'order_status';
    protected $primaryKey = 'order_status_id';

    public function order()
    {
        return $this->hasMany('App\Order', 'order_status_id');
    }

    public function orderItem()
    {
        return $this->belongsToMany('App\OrderItem', 'status_id');
    }

    public function scopeOpen($query) {
        return $query->where('order_status_id', '<', 500);
    }
}