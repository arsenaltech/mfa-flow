<?php
namespace App\Auth;
use Illuminate\Contracts\Hashing\Hasher as HasherInterface;

/**
 * Created by PhpStorm.
 * User: shabbir
 * Date: 3/28/17
 * Time: 5:21 PM
 */
class Hasher implements HasherInterface
{

    public function __construct()
    {
    }

    /**
     * Hash the given value.
     *
     * @param  string $value
     * @param  array $options
     * @return string
     */

    public function make($value, array $options = [])
    {
        return sha1($value);
    }

    /**
     * Check the given plain value against a hash.
     *
     * @param  string $value
     * @param  string $hashedValue
     * @param  array $options
     * @return bool
     */
    public function check($value, $hashedValue, array $options = [])
    {
        return sha1($value) == $hashedValue;
    }

    /**
     * Check if the given hash has been hashed using the given options.
     *
     * @param  string $hashedValue
     * @param  array $options
     * @return bool
     */
    public function needsRehash($hashedValue, array $options = [])
    {
        return false;
    }

    public function info($hashedValue){
        return $hashedValue;
    }

}