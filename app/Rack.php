<?php
/**
 * Created by PhpStorm.
 * User: shabbir
 * Date: 3/24/17
 * Time: 2:11 PM
 */

namespace App;


use App\Events\OrderReleased;
use Illuminate\Database\Eloquent\Model;

class Rack extends Model
{
    protected $table = 'rack_numbers';
    protected $primaryKey = 'rack_id';
    public $timestamps = false;

    public function order()
    {
        return $this->hasOne(Order::class, 'order_id', 'order_id');
    }

    public static function release($order_id) {
        Rack::whereOrderId($order_id)->update(['order_id'=> null, 'assigned_date'=> null]);
        event(new OrderReleased($order_id));
    }
}