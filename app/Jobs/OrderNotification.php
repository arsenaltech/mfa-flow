<?php

namespace App\Jobs;

use App\Order;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Log;

class OrderNotification implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;


    const SOCIETY_6_API_KEY = '608e58f80b170edca59e4bc0afc6d508';
    const SOCIETY_6_API_SECRET = 'e946fa0a2b389624289f054534593341';
    /**
     * @var Order
     */
    private $order;

    /**
     * Create a new job instance.
     *
     * @param Order $order
     */
    public function __construct(Order $order)
    {
        $this->order = $order;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {

        $order_id = $this->order->order_id;
        $data = '{"bol": ' . $order_id . '}';
        $shipping = $this->order->shipping;
        $signature = sha1(self::SOCIETY_6_API_SECRET . self::SOCIETY_6_API_KEY . $data);
        $response = Http::post("https://society6.com/api/v1/orders/" . $this->order->xid,
            ['key' => self::SOCIETY_6_API_KEY, 'signature' => $signature, 'data' => $data])->json();
        $response['order_id'] = $order_id;

        try {
            //print_r($response);
            if ($response['code'] == 200 || $response['code'] == 400) {
                $shipping->updated_to_api = 1;
                $shipping->error_flag = 0;
            } else {
                Log::channel('slack')->info('Posting order ', ['data' => $response]);
                $shipping->updated_to_api = 1;
                $shipping->error_flag = 1;
            }
            $shipping->save();
        } catch (Exception $e) {
            Log::channel('slack')->info('Posting order ', ['data' => $e]);
        }
    }
}
