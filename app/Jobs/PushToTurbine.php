<?php

namespace App\Jobs;

use App\ApiLog;
use App\Order;
use App\OrderItem;
use App\SkuMapping;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class PushToTurbine implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * @var Order
     */
    protected $order;
    private $user_id;
    /**
     * @var string
     */
    private $token;
    /**
     * @var int
     */
    private $account_id;
    /**
     * @var string
     */
    private $turbine_url;
    /**
     * @var string
     */
    private $mfa_url;

    /**
     * Create a new job instance.
     *
     * @param Order $order
     */
    public function __construct(Order $order)
    {
        $this->order = $order;
        $this->user_id = $order->user_id;

        if (config('app.env') == 'production') {
            $this->token = $this->idToken($this->user_id);
            $this->turbine_url = 'https://api.thedreamjunction.com/api/v3/orders';
            $this->mfa_url = 'https://www.marcofinearts.com/admin/api/update_status';
        } else {
            $this->token = '4a92b9857f4cf7e6dd8bfb730c5cce0f';
            $this->turbine_url = 'https://staging.thedreamjunction.com/api/v3/orders';
            $this->mfa_url = 'https://dev.marcofinearts.com/admin/api/update_status';
        }

    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $order = $this->order;
        if ($order->ProcessingItems()->count() > 0) {
            $this->pushOrder($order);
        } else {
            $order->order_status_id = OrderItem::where('order_id', $order->order_id)->get()->min('status_id');
            $order->save();
        }

    }

    private function pushOrder($order)
    {
        $response = Http::withHeaders([
            'Authorization' => 'Token token=' . $this->token
        ])->post($this->turbine_url, $this->mapping($order));

        if ($response->successful()) {
            foreach ($order->ProcessingItems as $orderItem) {
                $this->updateStatus($orderItem);
            }
            //Log::channel('slack')->info('Posting order ', ['response'=>$response->body()]);
        } else {
            Log::channel('slack')->info('Issue pushing order: ' . $order->order_id . " " . $response->body());
        }

    }


    private function updateStatus($orderItem)
    {
        $response = Http::asForm()->post($this->mfa_url,
            [
                'order_status_id' => '515',
                'order_info_id' => $orderItem->order_info_id,
                'order_id' => $orderItem->order_id,
                'comment' => 'Added to Turbine Queue'
            ]
        );
        return $response;
    }

    private function mapping($order)
    {
        $arr = [
            'type' => 'order',
            'account_zip' => '92704',
            'purchase_order' => $order->order_id,
            'order_reference_number' => $order->xid,
            'garments_provided' => false,
            'ship_provider' => $this->shippingCourier(),
            'ship_method' => $this->shippingMethod(),
            //'ship_from' => $this->shipFrom($order->shipping),
            'ship_to' => $this->shipTo($order->address),
            'shipping_label_url' => $this->shippingLabel($order),
            //'packing_slip_url' => $this->packingSlip($order),
            //'addtl_ship_docs_url' => '',
            'items' => $this->items($order->ProcessingItems),
        ];

        // Log::channel('slack')->info('Posting order ', ['data'=>$arr]);

        return $arr;
    }

    private function shippingCourier(){
        if(in_array($this->user_id, [2, 7591])){
            return 'Pre-Paid';
        }else{
            if($this->order->address->ship_to_country == 'US') {
                return 'USPS';
            }else{
                return 'Pre-Paid';
            }
        }
    }

    private function shippingMethod()
    {
        if(in_array($this->user_id, [2, 7591])){
            return 'Pre-Paid';

        }else{
            if($this->order->address->ship_to_country == 'US') {
                return 'FIRST';
            }else{
                return 'Pre-Paid';
            }
        }
    }

    private function shipFrom($shipping)
    {
        return [
            'first_name' => $shipping->ship_from_name,
            'company_name' => $shipping->ship_from_company,
            'address' => $shipping->ship_from_address,
            'address_2' => $shipping->ship_from_address2,
            'city' => $shipping->ship_from_city,
            'state' => $shipping->ship_from_state,
            'zip_code' => $shipping->ship_from_zip,
            'country' => $shipping->ship_from_country,
            'email' => $shipping->ship_from_email,
            'telephone' => $shipping->ship_from_phone,
        ];
    }

    private function shipTo($shipping)
    {
        return [
            'first_name' => $shipping->ship_to_name,
            'company_name' => $shipping->ship_to_company,
            'address' => $shipping->ship_to_address,
            'address_2' => $shipping->ship_to_address2,
            'city' => $shipping->ship_to_city,
            'state' => $shipping->ship_to_state,
            'zip_code' => $shipping->ship_to_zip,
            'country' => $shipping->ship_to_country,
            'email' => $shipping->ship_to_email,
            'telephone' => $shipping->ship_to_phone,
        ];
    }

    private function items($orderItems)
    {

        $items = array();

        foreach ($orderItems as $orderItem) {
            $designs = [];
            $optionText = json_decode($orderItem->optionstxt);
            $sku = $orderItem->sku ? $orderItem->sku : $optionText->Sku;
            $placement = data_get($optionText, 'placement') ?: '';
            $designs[] = ['placement' => $this->placement($placement),
                'art_url' => $this->artUrl($optionText->Sku, $orderItem),
                'thumbnail_url' => ($this->user_id == 8679 || $this->user_id == 8348) ? $this->artUrl($optionText->Sku, $orderItem) : $orderItem->thumb,
                'underbase' => $this->underBase($sku)];


            $neckTag = $this->neckTag($sku, $placement);
            if ($neckTag) {
                $designs[] = $neckTag;
            }


            if (Str::contains($optionText->Sku, [
                'CT21238',
                'CT20346',
                'CT21239',
                'CT21273',
                'CT21240',
                'CT21241',
                'CT21242',
                'CT21243'])) {
                $placement = 'front-back-neck';
            }


            if ($placement == 'front-back-neck' || $placement == 'front-back') {
                $designs[] = ['placement' => 'Back Center',
                    'art_url' => Storage::disk('s3')->temporaryUrl($this->user_id . '/backs/' . $optionText->Sku . '.png', now()->addDays(7)),
                    'thumbnail_url' => Storage::disk('s3')->temporaryUrl($this->user_id . '/backs/' . $optionText->Sku . '.png', now()->addDays(7)),
                    'underbase' => $this->underBase($sku)];
            }

            $sku = $this->skuMapping($sku);

            $items[] = [
                'customer_sku' => $sku,
                'item_reference_number' => $orderItem->order_info_id,
                'sku' => $sku,
                'name' => isset($optionText->Title) ? $optionText->Title : '',
                'description' => $this->itemDescription($optionText),
                'quantity' => $orderItem->quantity,
                'designs' => $designs,
            ];

            if($this->user_id == 8960 or $this->user_id == 9042){
                $items['custom_tags'][] = array('tag_code'=>$sku, 'tag_type'=>'FTG_HANG_UPC');
            }
        }
        return $items;
    }

    private function skuMapping($sku)
    {
        $vendor_sku = SkuMapping::where('sku', $sku)->where('user_id', $this->user_id)->first();
        if ($vendor_sku) {
            $sku = $vendor_sku->vendor_sku;
        }
        return $sku;
    }

    private function artUrl($sku, $orderItem)
    {
        if ($this->user_id == 2 || $this->user_id == 8342 || $this->user_id == 7591) {
            $art_url = $orderItem->original;
        } else if ($this->user_id == 8485) {
            $art_url = Storage::disk('s3')->temporaryUrl($this->user_id . '/' . Str::before($sku, '-') . '.png', now()->addDays(7));
        } else {
            $art_url = Storage::disk('s3')->temporaryUrl($this->user_id . '/' . $sku . '.png', now()->addDays(7));
        }
        return $art_url;
    }




    private function idToken($id)
    {
        $arr = array(
            2 => '4d8b548f18f8663614b358b0d9ed0980',  // S6
            7591 => 'affb2085f9232d66dc1e07c9774d9a2a',  // RedBubble
            7757 => '43f5e5a77c9bfb9c91c2109b2abd7673',  // Rooster
            8342 => '087c40ce83173377bab92a48d64597fd',  // S6 wholesale
            8348 => '7b23c01e6cd2de501defa5136e694519',  // Chive
            8485 => '691a8f6058ff9b4b522c3a305a1c58a2',  // ClassicDad
            8679 => 'f5bf6f8dec3f3ef1c38439da21a0b767',  // TeamChecked
            8783 => '34464046b80dd1d13b53954842ad2624',  // Calm Down
            8830 => 'bc2b77f95920ef8c869321c1aa3d74c3',  // Retro Rags Limited
            8960 => '0ca85c8048e3f830cab46cc3a23b3580',  // M&N
            9042 => 'bc540bc7fb239319ee47b0d77253bad5',  // M&N Wholesale
            8840 => '5ea6b5f56885eb206baee9c52c7a664c',  // Print For Partners
            8844 => '5ea6b5f56885eb206baee9c52c7a664c',
            8445 => '5ea6b5f56885eb206baee9c52c7a664c',
            8874 => '5ea6b5f56885eb206baee9c52c7a664c',
            8908 => '5ea6b5f56885eb206baee9c52c7a664c',
            8917 => '5ea6b5f56885eb206baee9c52c7a664c',
        );

        return $arr[$id];
    }

    private function shippingLabel($order)
    {
        $label_url = 'https://www.marcofinearts.com/admin/label/' . $this->encrypt($order->order_id);
        return $label_url;
    }

    private function packingSlip($order)
    {
        $order_txt = json_decode($order->ordertxt);
        return isset($order_txt->Receipt) ? $order_txt->Receipt : "";
    }

    private function itemDescription($item)
    {
        $description = "";
        if (isset($item->Personalization)) {
            $description = $item->Personalization;
        } else if (isset($item->Notes)) {
            $description = $item->Notes;
        }

        return $description;
    }

    private function placement($placement)
    {
        if ($placement == 'back-print') {
            return 'Back Center';
        }elseif ($placement == 'chest') {
            return 'Front Left Chest';
        }
        return 'Front Center';
    }

    private function underBase($sku)
    {
        if (Str::contains($sku, '-st-')) {
            return false;
        }
        return true;
    }

    private function neckTag($sku, $placement)
    {
        if ($this->user_id == 8348 || Str::contains($placement, 'neck')) {
            return [
                'placement' => 'Neck',
                'art_url' => Storage::disk('s3')->temporaryUrl($this->user_id . '/tags/' . $sku . '.png', now()->addDays(7)),
                'thumbnail_url' => Storage::disk('s3')->temporaryUrl($this->user_id . '/tags/' . $sku . '.png', now()->addDays(7)),
            ];
        }

        return false;
    }

    private function encrypt($simple_string)
    {
        return urlencode(openssl_encrypt($simple_string, "AES-128-CBC", "GEEK", 0, '1234567891011121'));
    }

}
