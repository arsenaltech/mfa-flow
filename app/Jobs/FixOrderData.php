<?php

namespace App\Jobs;

use App\Order;
use App\SkuMapping;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;

class FixOrderData implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * @var Order
     */
    /**
     * @var Order
     */
    protected $order;
    /**
     * @var string
     */
    private $mfa_url;

    /**
     * Create a new job instance.
     *
     * @param Order $order
     */
    public function __construct(Order $order)
    {
        $this->order = $order;

        if (config('app.env') == 'production') {
            $this->mfa_url = 'https://www.marcofinearts.com/admin/api/update_status';
        } else {
            $this->mfa_url = 'https://dev.marcofinearts.com/admin/api/update_status';
        }

    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        if ($this->order->user_id == 8348) {
            $this->returnCheck();
            $this->shippingMethodOverride();
        }

        if ($this->order->user_id == 1828) {
            $this->updatePackaging();
        }

        if ($this->order->user_id == 5808) {
            $this->orderToPrice();
        }

        if ($this->order->user_id == 1622) {
            $this->shippingMethodOverride();
        }

        if ($this->order->user_id == 7863) {
            $this->BAOverride();
        }

        if ($this->order->user_id == 4996) {
            $this->addBoxes();
        }

        if ($this->order->user_id == 8490) {
            $this->UpdateAccount();
        }

        if ($this->order->user_id == 8679) {
            $this->UpdateShipping();
        }

        if ($this->order->user_id == 7661) {
            $this->UpdateSize();
        }

        if ($this->order->user_id == 8960) {
            $this->UpdateMNItems();
        }
    }

    private function UpdateAccount()
    {
//        $this->order->user_id = $this->order->address->user_id = 8686;
//        $this->order->save();
//        $this->order->address->save();



        $valid_states = ['WA',
            'OR',
            'CA'];

        if (in_array($this->order->address->ship_to_state,$valid_states)) {
            $this->order->user_id = $this->order->address->user_id = 9013;
            $this->order->save();
            $this->order->address->save();
        }

    }


    private function UpdateMNItems(){

        $totalChange = 0;
        foreach ($this->order->orderItems as $orderItem) {

            $options_txt = json_decode($orderItem->optionstxt, true);
            $sku_mapping = $this->skuCost(data_get($options_txt,'Sku'));

            if($sku_mapping){
                $orderItem->type    = $sku_mapping->type;
                $orderItem->width   = $sku_mapping->width;
                $orderItem->height  = $sku_mapping->height;

                if ($sku_mapping->placement) {
                    $options_txt['placement'] = $sku_mapping->placement;
                }

                if ($sku_mapping->finishing) {
                    $options_txt['Finishing'] = $sku_mapping->finishing;
                }

                if ($sku_mapping->framing) {
                    $options_txt['Framing'] = $sku_mapping->framing;
                }

                if ($sku_mapping->matting) {
                    $options_txt['Matting'] = $sku_mapping->matting;
                }

                $orderItem->optionstxt = json_encode($options_txt);
                $orderItem->thumb = "https://mfa-asset.s3.amazonaws.com/8960/thumbs/".$sku_mapping->thumb;
                $orderItem->cost = $sku_mapping->cost;

                if($sku_mapping->type == 'Tshirt'){
                    $orderItem->status_id = config('mfa.turbine_status');
                }
                $orderItem->save();
                $totalChange += $sku_mapping->cost * $orderItem->quantity;

                $this->order->order_status_id = config('mfa.turbine_status');
                $this->order->save();

            }

        }

        $this->totalUpdate($totalChange);
    }

    private function returnCheck()
    {
        $response = Http::get('https://afcff1e4aba6c6908110bb2727e0d31e:0716da63d1cce55a683dd9ed62594592@thechivery.myshopify.com/admin/api/2020-07/orders/' . $this->order->xid . '.json');


        if ($response->successful()) {
            $order = $response->json();
            if (isset($order['order'])) {
                $order = $order['order'];
                if (isset($order['tags']) && $order['tags'] == 'returnly_exchange') {
                    foreach ($this->order->orderItems as $orderItem) {
                        Http::asForm()->post($this->mfa_url,
                            [
                                'order_status_id' => '230', // Put it in hold
                                'order_info_id' => $orderItem->order_info_id,
                                'order_id' => $this->order->order_id,
                                'comment' => $order['note']
                            ]
                        );
                    }
                }
            }
        }
    }


    private function orderToPrice()
    {
        $totalChange = 0;
        foreach ($this->order->orderItems as $orderItem) {
            if ($orderItem->cost <= 0) {
                $totalChange += $this->itemCost($orderItem);
            }
        }
        $this->totalUpdate($totalChange);
    }

    private function UpdateSize()
    {
        foreach ($this->order->orderItems as $orderItem) {
            $sku = data_get(json_decode($orderItem->optionstxt),'Sku');
            if (Str::contains($sku, 'FAP')) {
                switch ($sku) {
                    case 'FAP-8x10':
                        $orderItem->width = 5.2;
                        $orderItem->height = 6.99;
                        break;
                    case 'FAP-10x10':
                        $orderItem->width = 7;
                        $orderItem->height = 7;
                        break;
                    case 'FAP-13x19':
                        $orderItem->width = 8.5;
                        $orderItem->height = 11.3;
                        break;
                    case 'FAP-20x20':
                        $orderItem->width = 14;
                        $orderItem->height = 14;
                        break;
                    case 'FAP-18x24':
                        $orderItem->width = 12.95;
                        $orderItem->height = 17.27;
                        break;
                    case 'FAP-24x36':
                        $orderItem->width = 17.06;
                        $orderItem->height = 22.75;
                        break;
                    case 'FAP-30x30':
                        $orderItem->width = 22;
                        $orderItem->height = 22;
                        break;

                }

                $orderItem->save();
            }
        }

    }



    private function shippingMethodOverride()
    {
        $overrideShipping = false;

        if ($this->order->shipping->service == 'FEDEX_GROUND') {
            foreach ($this->order->orderItems as $orderItem) {
                // FAA
                if (Str::contains($orderItem->type, 'Rolled')) {
                    $overrideShipping = 'SMART_POST';
                }else{
                    $overrideShipping = 'GROUND_HOME_DELIVERY';
                }
            }
        }


        foreach ($this->order->orderItems as $orderItem) {
            // Chive
            if (Str::contains($orderItem->type, 'Subscription')) {
                $overrideShipping = 'SMART_POST';
            }
        }

        if ($overrideShipping) {
            $this->order->shipping->service = $overrideShipping;
            $this->order->shipping->save();
        }



    }

    private function updatePackaging(){

        $overrideShipping = false;

        foreach ($this->order->orderItems as $orderItem) {
            $finishing = data_get(json_decode($orderItem->optionstxt),'Finishing');

            if ($finishing == 'Rolled') {
                $overrideShipping = true;
            }
        }

        if ($overrideShipping) {
            $this->order->payment->packaging = 5;
            $this->order->payment->total += $this->order->payment->packaging;
            $this->order->payment->save();
        }
    }

    private function updateShipping()
    {


        $this->order->shipping->service = 'SMART_POST';
        $this->order->shipping->save();

        $shipping = 2.95 + $this->order->orderItems->count() * 2;
        $this->order->payment->shipping = $shipping;
        $this->order->payment->total = $this->order->payment->sub_total + $shipping + $this->order->payment->tax + $this->order->payment->packaging - $this->order->payment->discount;
        $this->order->payment->save();


    }

    private function BAOverride()
    {
        $overrideShipping = true;
        foreach ($this->order->orderItems as $orderItem) {
            if ($orderItem->width > 6 or $orderItem->heigh > 6) {
                $overrideShipping = false;
                break;
            }
        }
        if ($overrideShipping && $this->order->orderItems) {
            $this->order->payment->shipping = 7;
            $this->order->payment->total = $this->order->payment->sub_total + $this->order->payment->shipping + $this->order->payment->tax + $this->order->payment->packaging - $this->order->payment->discount;
            $this->order->payment->save();
        }
    }

    private function addBoxes()
    {
        $box = [];
        foreach ($this->order->orderItems as $orderItem) {
            $box[] = $this->mintedMapping($orderItem);
        }
        if (!empty($box)) {
            $this->order->shipping->box_info = implode(',', $box);
            $this->order->shipping->save();
        }
    }


    private function skuCost($sku)
    {

        // sku exceptions
        if ($this->order->user_id == 8348) {
            $sku = Str::after($sku, '-');
        }
        return SkuMapping::where('sku', $sku)->where('user_id', $this->order->user_id)->first();
    }


    private function itemCost($orderItem)
    {
        $cost = 0;
        if ($this->order->user_id == 5808) {
            $sku = $this->threadlessMapping($orderItem);
        }

        if (!empty($sku)) {
            $orderItem->sku = $sku;
            $cost = $this->skuCost($sku);
            $orderItem->cost = $cost ? $cost->cost * $orderItem->quantity : 0;
            $orderItem->save();
            $cost = $orderItem->cost;
        }

        return $cost;
    }

    /**
     * @param $totalChange
     */
    private function totalUpdate($totalChange)
    {
        if ($totalChange) {
            $this->order->payment->sub_total = $this->order->payment->sub_total + $totalChange;
            $this->order->payment->total = $this->order->payment->sub_total + $this->order->payment->shipping + $this->order->payment->tax + $this->order->payment->packaging - $this->order->payment->discount;
            $this->order->payment->save();
        }
    }


    private function threadlessMapping($orderItem)
    {
        $optionText = json_decode($orderItem->optionstxt);
        $sku = [$orderItem->type, round($orderItem->width) . round($orderItem->height)];
        if (isset($optionText->Finishing)) {
            $sku[] = $optionText->Finishing;
        }

        if ($orderItem->type == 'card') {
            $sku[] = $orderItem->quantity;
        }

        return $this->clean(implode('-', $sku));
    }

    private function clean($string)
    {
        $string = str_replace(' ', '-', $string); // Replaces all spaces with hyphens.
        return preg_replace('/[^A-Za-z0-9\-]/', '', strtolower($string)); // Removes special chars.
    }


    private function mintedMapping($orderItem)
    {
        $optionText = json_decode($orderItem->optionstxt);

        $arr = ['Framed' => ['5x7' => '12.25x10.24x2.4375',
            '8x10' => '13.25x15.25x2.4374',
            '8x8' => '13.25x15.25x2.4375',
            '11x11' => '18.375x21.75x2.4375',
            '11x14' => '18.375x21.75x2.4376',
            '16x16' => '26.25x27x2.4375',
            '16x20' => '26.25x27x2.4375',
            '18x24' => '33.5x42.625x2.4375',
            '24x24' => '33.5x42.625x2.4376',
            '24x30' => '33.5x42.625x2.4377',
            '54x40' => '59x48x3',
            '44x44' => '51.875x52.5x3',
            '30x30' => '38x38.3125x3.125',
            '30x40' => '37.625x48x2.875'
        ],
            'Rolled_bkp' => ['5x7' => '9.75x12.25x1',
                '8x8' => '9.75x12.26x1',
                '8x10' => '9.75x12.27x1',
                '11x11' => '12.75x15x1',
                '11x14' => '12.75x16x1',
                '16x16' => '4x4x20.75',
                '16x20' => '4x4x20.76',
                '18x24' => '4x4x20.77',
                '24x24' => '6x6x34',
                '30x30' => '6x6x35',
                '30x40' => '6x6x36',
                '44x44' => '6x6x46',
                '54x40' => '6x6x47',
                '60x40' => '6x6x48',]
        ];

        if (isset($arr[$optionText->Finishing][(int)$orderItem->width . 'x' . (int)$orderItem->height])) {
            return $arr[$optionText->Finishing][(int)$orderItem->width . 'x' . (int)$orderItem->height];
        } else if (isset($arr[$optionText->Finishing][(int)$orderItem->height . 'x' . (int)$orderItem->width])) {
            return $arr[$optionText->Finishing][(int)$orderItem->height . 'x' . (int)$orderItem->width];
        }

        return null;
    }


    private function skuToPrice()
    {
        $totalChange = 0;
        foreach ($this->order->orderItems as $orderItem) {
            $optionText = json_decode($orderItem->optionstxt);
            $sku = isset($optionText->Sku) ? $optionText->Sku : $orderItem->sku;
            if ($orderItem->cost <= 0 && !empty($sku)) {
                $cost = $this->skuCost($sku);
                $orderItem->cost = $cost ? $cost->cost * $orderItem->quantity : 0;
                $orderItem->save();
                $totalChange += $orderItem->cost;
            }
        }
        $this->totalUpdate($totalChange);
    }

}
