<?php

namespace App\Jobs;

use App\Notifications\ShopifyOrder;
use App\Notifications\SyncError;
use App\Order;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Notification;

class ShopifyNotification implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $orders;
    private $user_id;
    private $store;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($orders ,$user_id, $user)
    {
        $this->orders = $orders;
        $this->user_id = $user_id;
        $this->store = $user;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $missing_order = $missing_item = [];

        foreach ($this->orders as $shopify_order){

            echo "\n checking: ".$shopify_order->id ." and ".$shopify_order->name;

            $order = Order::with('orderItems')
                ->where('user_id',$this->user_id)
                ->where(function ($query) use ($shopify_order) {
                    $query->where('xid', $shopify_order->name)
                        ->orWhere('xid', $shopify_order->id);
                })
                ->first();

            if($order){
                if(count($shopify_order->line_items) != $order->orderItems->count()){
                    $missing_item[] =  $shopify_order->name;
                }
            }else{
                $missing_order[] = $shopify_order->name;
            }
        }

        if($missing_order or $missing_item) {
            Notification::route('mail', 'production@thecollectivemarketing.com')
                ->route('slack', 'https://hooks.slack.com/services/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')
                ->notify(new ShopifyOrder($missing_order, $missing_item, $this->store));
        }

    }
}
