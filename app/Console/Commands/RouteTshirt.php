<?php

namespace App\Console\Commands;

use App\Jobs\PushToTurbine;
use App\Order;
use Illuminate\Console\Command;

class RouteTshirt extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'RouteTshirt';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();

        if(config('app.env') == 'production'){
            $this->mfa_url = 'https://www.marcofinearts.com/admin/api/update_status';
        } else{
            $this->mfa_url = 'https://dev.marcofinearts.com/admin/api/update_status';
        }
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $orders = Order::with('ProcessingItems', 'address', 'shipping')
            ->whereIn('order_status_id', [config('mfa.turbine_status')])
            ->whereIn('user_id', [2,8342,7757,8348,8485,7591,8679,8774, 8783, 8830, 8840, 8844, 8445,8874, 8908, 8917,  8960 ])
            //->whereIn('order_id', [  3586214])
            ->take(50)
            ->get();

        foreach ($orders as $order) {
            //echo "Pushing:".$order->order_id;
            dispatch(new PushToTurbine($order));
//            foreach ($order->ProcessingItems as $orderItem) {
//                $orderItem->status_id = 315; // Change to Printing
//                $orderItem->save();
//            }
        }
    }
}
