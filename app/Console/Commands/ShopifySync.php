<?php

namespace App\Console\Commands;

use App\Jobs\ShopifyNotification;
use App\Order;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Http;


class ShopifySync extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'shopify-sync';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';
    /**
     * @var array[]
     */
    private $shopify_users;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();

        $this->shopify_users = array(
            8526 => array('4a1036c073bc2f8a023e40cfa45cf1ac', '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', 'relm-artist', 48626270371),
            8792 => array('e8e9769228aa932d4292fc6266e137bc', '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', 'cosmicbackground', 62520524969),
        );

    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        foreach ($this->shopify_users as $key => $user) {


            $shopify_domain = "https://$user[2].myshopify.com";
            $order_url = $shopify_domain . '/admin/api/2021-07/orders.json?fields=id,name,created_at,line_items&created_at_min='.date("Y-m-d",strtotime("-1 days"));

            //echo $order_url;

            $response = Http::withHeaders([
                'X-Shopify-Access-Token' => $user[1]])
                ->accept('application/json')
                ->get($order_url);

            if ($response->successful()) {
                $orders = collect(json_decode($response->body())->orders);
                dispatch(new ShopifyNotification($orders, $key, $user));
            }

        }


    }
}
