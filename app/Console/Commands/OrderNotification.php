<?php

namespace App\Console\Commands;

use App\Order;
use Illuminate\Console\Command;

class OrderNotification extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'OrderNotification';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $orders = Order::join('shipping', 'order.order_id','=','shipping.order_id')
                ->whereIn('order.user_id', [2,8342,8864])
                ->where('order.order_status_id', 520)
                ->where('shipping.updated_to_api', 0)
                ->where('xid', 'not like', '%Reship%')
                ->get();

        foreach ($orders as $order){
            dispatch(new \App\Jobs\OrderNotification($order));
        }
    }
}
