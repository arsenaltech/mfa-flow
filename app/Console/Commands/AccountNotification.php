<?php

namespace App\Console\Commands;

use App\Account;
use App\Notifications\NewAccountCreated;
use App\Notifications\ShopifyOrder;
use App\Order;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Notification;

class AccountNotification extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'account-notification';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();

    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $accounts = Account::where('user_id', ">",  '8910')
            ->where('regdate', '>', date("Y-m-d",strtotime("-1 days")))
            ->get();

        foreach ($accounts as $account){
            Notification::route('mail', 'accountsonboarding@marcofinearts.com')
                ->route('mail', '261abac9.marcofinearts.com@amer.teams.ms')
                ->notify(new NewAccountCreated($account));
        }
    }
}
