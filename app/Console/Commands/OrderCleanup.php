<?php

namespace App\Console\Commands;

use App\Jobs\FixOrderData;
use App\Order;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Cache;

class OrderCleanup extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'OrderCleanup';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $order_id = Cache::get('order_id', function () {
            return Order::where('order_date','>', Carbon::yesterday())->min('order_id');
        });


        $orders = Order::with('orderItems', 'address', 'shipping', 'payment')
                    ->where('order_id', '>', $order_id)
                    //->whereOrderId( 5849026)
                    ->whereIn('user_id', [1622, 1828, 4996, 5808, 7863, 7661, 8348, 8469, 8679 , 8490, 8960] )
                    ->get();

        foreach ($orders as $order){
            //echo "Processing ". $order->order_id;
            dispatch(new FixOrderData($order));
            $order_id = $order->order_id;
        }

        Cache::put('order_id',$order_id);

    }
}
