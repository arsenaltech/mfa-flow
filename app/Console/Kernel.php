<?php

namespace App\Console;

use App\Console\Commands\AccountNotification;
use App\Console\Commands\OrderCleanup;
use App\Console\Commands\OrderNotification;
use App\Console\Commands\RouteTshirt;
use App\Console\Commands\ShopifySync;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        RouteTshirt::class,
        OrderCleanup::class,
        OrderNotification::class,
        ShopifySync::class,
        AccountNotification::class
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->command('RouteTshirt')->everyMinute();
        $schedule->command('OrderCleanup')->everyMinute();
        $schedule->command('OrderNotification')->everyMinute();
        $schedule->command('shopify-sync')->daily();
        $schedule->command('account-notification')->timezone('America/Chicago')->daily();
    }

    /**
     * Register the Closure based commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        require base_path('routes/console.php');
    }
}
