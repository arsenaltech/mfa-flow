<?php
/**
 * Created by PhpStorm.
 * User: shabbir
 * Date: 3/17/17
 * Time: 7:43 PM
 */

namespace App;


use App\Events\OrderConsolidated;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Order extends Model
{
    protected $table = 'order';
    protected $primaryKey = 'order_id';
    public $timestamps = false;
    //public $appends=['totalItemQty', 'totalQtyInBin'];


    public function orderItems()
    {
            return $this->hasMany(OrderItem::class, 'order_id')
                        ->whereRaw("status_id NOT IN (0)" )
                        ->orderBy('status_id', 'DESC');
    }

    public function ProcessingItems()
    {
        return $this->hasMany(OrderItem::class, 'order_id')
            ->where("status_id", '=', '310' );
    }

    public function account(){
        return $this->belongsTo(Account::class, 'user_id','user_id');
    }

    public function address(){
        return $this->belongsTo(OrderAddress::class, 'order_id','order_id');
    }

    public function shipping(){
        return $this->belongsTo(Shipping::class, 'order_id','order_id');
    }

    public function payment(){
        return $this->belongsTo(Payment::class, 'order_id','order_id');
    }

    public function rack()
    {
        return $this->belongsTo(Rack::class, 'order_id', 'order_id');
    }

    public function itemsCount()
    {
        return $this->hasOne(OrderItem::class, 'order_id', 'order_id')
            ->selectRaw('order_id, sum(quantity) as total_quantity, sum(quantity_consolidated) as total_quantity_consolidated')
            ->groupBy('order_id');
        // replace module_id with appropriate foreign key if needed
    }

    public function allItems()
    {
        return $this->hasMany(OrderItem::class, 'order_id');
        // replace module_id with appropriate foreign key if needed
    }

    public function status()
    {
        return $this->hasOne(OrderStatus::class, 'order_status_id', 'order_status_id');
    }

    public function assignRack() {
        DB::transaction(function () {
            //find empty rack
            $rack = Rack::whereNull('order_id')->first();
            //update rack table
            $rack->order_id = $this->order_id;
            $rack->assigned_date = Carbon::now();
            $this->rack = $rack;
            $rack->save();


        });
    }

    public function statusHistory()
    {
        return $this->hasOne('App\OrderStatusHistory', 'order_id', 'order_id');
    }

    public function scopeExcludeNonShippable($query) {
        return $query
            ->whereRaw("order_status_id NOT IN (SELECT order_status_id FROM order_status where order_state IN ('Hold', 'Cancelled'))" );

    }

    public function scopeNotShipped($query) {
        return $query
            ->where("order_status_id", "<", 500 );

    }

    public function scopeLate($query) {
        return $query->where('estimated_ship_date', '<', Carbon::today('America/Los_Angeles'));
    }



}
