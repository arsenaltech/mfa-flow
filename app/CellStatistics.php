<?php
/**
 * Created by PhpStorm.
 * User: shabbir
 * Date: 4/7/17
 * Time: 4:20 PM
 */

namespace App;



use Carbon\Carbon;
use Illuminate\Support\Facades\Redis;

class CellStatistics
{

    public function getCount($cell_id, $user_id) {
        return Redis::get('itemCount_'.$user_id.'_'.$cell_id);

    }

    public function incrementCount($cell_id, $user_id) {
        Redis::incrBy('itemCount_'.$user_id.'_'.$cell_id, 1);
        Redis::expireAt('itemCount_'.$user_id.'_'.$cell_id, Carbon::tomorrow('America/Los_Angeles')->timestamp);
        return $this->getCount($cell_id, $user_id);
    }
}