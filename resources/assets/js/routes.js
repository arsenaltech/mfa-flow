import VueRouter from 'vue-router';

let routes = [
  {
    path: '/',
    component: require('./components/CellSelection.vue')
  },
  {
    path: '/consolidation',
    component: require('./components/Consolidation.vue')
  },
  {
    path: '/ready-to-ship/:store_id',
    component: require('./components/ReadyToShip.vue')
  }
];

export default new VueRouter({
  routes,
  linkActiveClass: 'is-active'
});