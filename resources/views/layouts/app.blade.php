<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'EC Orderflow') }}</title>

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.2/animate.min.css">

    <style>
        #itemBarcodeBox{
            margin-bottom: 10px;
        }
        .group-heading{
            border-bottom: 1px solid #3097d1;
            margin-bottom: 10px;

        }

        .row-no-gutter {
            margin-right: 0;
            margin-left: 0;
        }

        .row-no-gutter [class*="col-"] {
            padding-right: 0;
            padding-left: 0;
        }

        .card {
            background: #fff;
            position: relative;
            -webkit-box-shadow: 0px 1px 10px 0px rgba(207,207,207,1);
            -moz-box-shadow: 0px 1px 10px 0px rgba(207,207,207,1);
            box-shadow: 0px 1px 10px 0px rgba(207,207,207,1);
            -webkit-transition: all 0.5s ease;
            -moz-transition: all 0.5s ease;
            -ms-transition: all 0.5s ease;
            -o-transition: all 0.5s ease;
            transition: all 0.5s ease;
            margin-bottom: 25px;
        }

        .bin-box {
            background: #3D6AA2;
            position: relative;
            z-index: 2;
            color: #fff;
            padding: 10px;

        }
        .red .bin-box{
            background-color: red;
        }
        .green .bin-box{
            background-color: green;
        }
        .yellow .bin-box{
            background-color: #cece00;
        }
        .bin-number {
            font-size: 40px;
            font-weight: bold;
            line-height: 40px;
        }

        .days .row [class*="col-"]:nth-child(2) .day  {
            border-width: 0 1px 0 1px;
            border-style: solid;
            border-color: #eaeaea;
        }

        .days .row [class*="col-"] {
            -webkit-transition: all 0.5s ease;
            -moz-transition: all 0.5s ease;
            -ms-transition: all 0.5s ease;
            -o-transition: all 0.5s ease;
            transition: all 0.5s ease;
        }

        .days .row [class*="col-"]:hover{
            background: #eaeaea;
        }

        .day {
            padding: 10px 0px;
            text-align: center;

        }

        .day h1 {
            font-size: 13px;
            margin: 9px 0px;
        }

        .panel-body{
            text-align: -webkit-center;
        }
        .panel-default .panel-heading{
            background-color: #f5f5f5;
            font-weight: bold;
        }
        .binnable .panel-heading{
            color: #fff;
            background-color: #337ab7;
            border-color: #2e6da4;
        }
        .binnable .panel{
            border-color: #2e6da4;
        }
        .na{
            opacity: 0.3;
        }
        .na .panel-heading{
            color: #fff;
            background-color: gray;
            border-color: #2e6da4;
        }
        .na .panel{
            border-color: gray;
        }

        .group-heading h4{
            font-size: 2em;

        }
        .huge{
            font-size: 40px;
        }
        .glyphicon.larger{
            font-size: 4.2em;
        }
        .glyphicon.medium{
            font-size: 2em;
        }
        .panel-footer,.panel-heading{
            white-space: nowrap;
            overflow: hidden;
            text-overflow: ellipsis;
        }

        .glyphicon.spinning {
            animation: spin 1s infinite linear;
            -webkit-animation: spin2 1s infinite linear;
        }
        .book .glyphicon:before{
            content: "\e032";
        }
        .lux .glyphicon:before{
            content: "\e044";
        }

        .hardbound .glyphicon:before{
            content: "\e043";
        }
        .dysub .glyphicon:before{
            content: "\e104";
        }
        .acrylic .glyphicon:before{
            content: "\e237";
        }
        .art .glyphicon:before{
            content: "\e060";
        }
        .consolidation .glyphicon:before{
            content: "\e028";
        }
        .shipping .glyphicon:before{
            content: "\e102";
        }
        .confirmation{
            font-size: 50px;
            float: left;
            padding-right: 15px;
            color: #ff3860;
        }
        .cell-state{
            font-size: 30px;
        }


        @keyframes spin {
            from { transform: scale(1) rotate(0deg); }
            to { transform: scale(1) rotate(360deg); }
        }

        @-webkit-keyframes spin2 {
            from { -webkit-transform: rotate(0deg); }
            to { -webkit-transform: rotate(360deg); }
        }




    </style>

    <!-- Scripts -->
    <script>
        window.Laravel = {!! json_encode([
            'csrfToken' => csrf_token(),
        ]) !!};
    </script>
</head>
<body>
    <div id="app">
        <nav class="navbar navbar-default navbar-static-top">
            <div class="container">
                <div class="navbar-header">

                    <!-- Collapsed Hamburger -->
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
                        <span class="sr-only">Toggle Navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>

                    <!-- Branding Image -->
                    <a class="navbar-brand" href="{{ url('/') }}">
                        {{ config('app.name', 'MFA Orderflow') }}
                    </a>
                </div>

                <div class="collapse navbar-collapse" id="app-navbar-collapse">
                    <!-- Left Side Of Navbar -->
                    <ul class="nav navbar-nav">
                        &nbsp;
                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="nav navbar-nav navbar-right">
                        <!-- Authentication Links -->
                        @if (Auth::guest())
                            <li><a href="{{ route('login') }}">Login</a></li>
                        @else
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                    {{ Auth::user()->name }} <span class="caret"></span>
                                </a>

                                <ul class="dropdown-menu" role="menu">

                                    <li>
                                        <a href="{{ route('logout') }}"
                                            onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                            Logout
                                        </a>

                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            {{ csrf_field() }}
                                        </form>
                                    </li>

                                </ul>
                            </li>
                        @endif
                    </ul>
                </div>
            </div>
        </nav>

        @yield('content')
    </div>
    <script>
    <?php
            foreach(config('mfa') as $key=>$value) {
                echo "var $key = '$value'; \n\r";
            }
    ?>
    </script>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}"></script>

    <script>

        $("#btn1, #btn4").click(function(){
            $("#product-list").slideDown("slow");
            $("#barcode").attr("placeholder","Item number or sku");
            $("#order-number-blk").show();
        })

        $("#btn2").click(function(){
            $target = $(".col-sm-3:first");
            $target.hide('slow', function(){ $target.remove(); });
        })

        $("#btn3").click(function(){
            $target =$('#search-bar');
            $target.removeClass().addClass('shake animated').one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function(){
                $(this).removeClass();
            });
            $target.addClass('has-error');
        })
    </script>
</body>
</html>
