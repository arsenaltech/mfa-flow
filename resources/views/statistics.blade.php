@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        @foreach ($data as $key=>$row)
            <div class="col-sm-3">
                <div class="card animated bounceIn">
                    <div class="bin-box">
                        <div class="pull-left">
                            <h4>{{ $row->user_name }}</h4>
                        </div>
                        <div class="pull-right">
                            <div class="bin-number">{{ $row->order_count }}</div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
        @endforeach
    </div>
@endsection
