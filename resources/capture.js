var system = require('system');
var args = system.args;

var fs = require('fs');

function getFileUrl(str) {
  var pathName = fs.absolute(str).replace(/\\/g, '/');
  // Windows drive letter must be prefixed with a slash
  if (pathName[0] !== "/") {
    pathName = "/" + pathName;
  }
  return encodeURI("file://" + pathName);
};

var page = require('webpage').create();
page.open(getFileUrl(args[1]), function() {
  page.render(args[2]);
  phantom.exit();
});