<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 27/4/16
 * Time: 5:39 PM
 */

return array(
	'mediaUrl' => env('MEDIA_URL','//img.marcofinearts.com/'),
    'turbine_status'=> 310,
    'PARTIALLY_IN_BIN' => 451,
    'WAITING_IN_BIN' => 452,
    'CONSOLIDATED' => 453,

);
